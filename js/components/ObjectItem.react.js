
var React = require('react');
var ObjActions = require('../actions/ObjActions');

var ObjectItem = React.createClass({
  render: function() {
    return (
      <li>
        <input type="checkbox"
               checked={this.props.isSelected}
               onChange={this._onChange} />
        <span>{this.props.name}</span>
      </li>
    );
  },

  _onChange: function(e) {
    var isSelected = e.target.checked,
        padding = {objId: this.props.id, funcId: this.props.parentFunc};
    if ( isSelected ) {
      ObjActions.select(padding);
    } else {
      ObjActions.unselect(padding);
    }
  }

});

module.exports = ObjectItem;
