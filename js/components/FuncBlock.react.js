
var classNames = require('classnames');
var React = require('react');

var ObjectItem = require('./ObjectItem.react');
var FuncActions = require('../actions/FuncActions');

var FuncBlock = React.createClass({

  render: function() {
    var objItems = this.props.objs.map((function (obj) {
      return <ObjectItem
                  id={obj.id}
                  name={obj.name}
                  isSelected={obj.isSelected}
                  parentFunc={this.props.func.id}
                  />
    }).bind(this));

    var _klassHidVis = {
      'is_collapsed': this.props.isCollapsed,
      'is_expanded': !this.props.isCollapsed
    };
    var klassObjList = classNames(
      'func-obj__objects-list',
      _klassHidVis
    );
    var klassExpandButton = classNames(
      'func-obj__expand-button',
      _klassHidVis
    );
    var klassFuncBlockHeader = classNames(
      'func-obj__func-header',
      _klassHidVis
    );

    return (
        <div>
          <div className={klassFuncBlockHeader}>
            <span onClick={this._toggleCollapse} className={klassExpandButton}></span>
            <span>{this.props.func.name}</span>
          </div>
          <ul className={klassObjList}>
            {objItems}
          </ul>
        </div>
    );
  },
  _toggleCollapse: function() {
    FuncActions.toggleCollapse(this.props.func.id)
  }
});

module.exports = FuncBlock;
