
var React = require('react');
var FuncBlock = require('./FuncBlock.react');
var AppStore = require('../stores/AppStore');
var RemoteActions = require('../actions/RemoteActions');

function getAppState() {
  return {
    funcBlocks: AppStore.getFuncBlocks()
  };
}

var RootApp = React.createClass({

  getInitialState: function() {
    return getAppState();
  },

  componentWillMount: function () {
    RemoteActions.fetchInitialDataFromRemote();
  },

  componentDidMount: function() {
    AppStore.addChangeListener(this._onChange);
  },

  componentWillUnmount: function() {
    AppStore.removeChangeListener(this._onChange);
  },

  render: function() {
    var funcBlocks = this.state.funcBlocks.map(function (block) {
      return (
        <li className="func-obj__func-block">
          <FuncBlock func={block.func}
                     objs={block.objs}
                     isCollapsed={block.isCollapsed} />
        </li>
      );
    });
    return (
      <td>
        <ul className="func-obj">{funcBlocks}</ul>
      </td>
    );
  },

  _onChange: function() {
    this.setState(getAppState());
  }

});

module.exports = RootApp;
