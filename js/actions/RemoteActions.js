
var AppDispatcher = require('../dispatcher/AppDispatcher');
var AppConstants = require('../constants/AppConstants');
var RemoteAPI = require('../api/RemoteAPI');

var RemoteActions = {
  fetchInitialDataFromRemote: function() {
    AppDispatcher.dispatch({
      actionType: AppConstants.START_FETCH_INITIAL_DATA_FROM_REMOTE
    });

    RemoteAPI.fetchInitialData().then(function (data) {
      AppDispatcher.dispatch({
        actionType: AppConstants.INITIAL_DATA_FROM_REMOTE_READY,
        data: data
      });
    });
  },
  flushDataToJiraField: function() {
    AppDispatcher.dispatch({
      actionType: AppConstants.SUBMIT_JIRA_FORM
    });
  }
};

module.exports = RemoteActions;
