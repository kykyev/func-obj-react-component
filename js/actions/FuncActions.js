
var AppDispatcher = require('../dispatcher/AppDispatcher');
var AppConstants = require('../constants/AppConstants');

var FuncActions = {
  toggleCollapse: function (funcId) {
    AppDispatcher.dispatch({
      actionType: AppConstants.FUNC_TOGGLE_COLLAPSE,
      funcId: funcId
    });
  }
};

module.exports = FuncActions;
