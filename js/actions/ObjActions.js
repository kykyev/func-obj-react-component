
var AppDispatcher = require('../dispatcher/AppDispatcher');
var AppConstants = require('../constants/AppConstants');

var ObjActions = {
  select: function (padding) {
    AppDispatcher.dispatch({
      actionType: AppConstants.OBJ_SELECT_ITEM,
      obj: padding
    });
  },
  unselect: function (padding) {
    AppDispatcher.dispatch({
      actionType: AppConstants.OBJ_UNSELECT_ITEM,
      obj: padding
    });
  }
};

module.exports = ObjActions;
