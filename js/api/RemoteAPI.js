
var work_stuff_cf = require('../constants/AppConstants').extra.work_stuff_cf;
var URL = require('../constants/AppConstants').extra.func_obj_map_url;
var $ = jQuery;

var RemoteAPI = {};

RemoteAPI.fetchInitialData = function () {
                        
    var ajaxPromise = new Promise(function(resolve, reject) {
      $.ajax({
        url: URL,
        type: "GET",
        dataType : "json",
        success: function(data) {
          resolve(data);
        }
      });
    });

    var fieldPromise = new Promise(function(resolve, reject) {
      $(function() {
        var fieldContent = $('#' + work_stuff_cf).val() || "[]";
        var selectedItems = JSON.parse(fieldContent);
        resolve(selectedItems)
      });        
    });

    return new Promise(function(resolve, reject) {
      Promise.all([ajaxPromise, fieldPromise]).then((vals) => {
        resolve({
          funcBlocks: vals[0],
          selectedItems: vals[1]
        });
      });
    });
};

module.exports = RemoteAPI;
