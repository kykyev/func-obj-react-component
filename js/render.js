var React = require('react');
var RootApp = require('./components/RootApp.react');
var action = require('./actions/RemoteActions');
var edit_form_submit_event = require('./constants/AppConstants').extra.edit_form_submit_event;
var $ = jQuery;


function render_component(container) {
    
    if (container === null) return;
    
    $(document).bind(edit_form_submit_event, (evt) => {
        action.flushDataToJiraField();
    });

    React.render(
        <RootApp />,
        container
    );
}

module.exports = render_component;
