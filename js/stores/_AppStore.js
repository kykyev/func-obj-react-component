
var work_stuff_cf = require('../constants/AppConstants').extra.work_stuff_cf;

var ext = {};

ext.buildFuncBlockIndex = (funcBlocks) => {
  var index = {};
  funcBlocks.forEach((block) => {
    index[block.func.id] = {
      block: block,
      obj: {}
    };
    block.objs.forEach((objItem) => {
      index[block.func.id].obj[objItem.id] = objItem;
    });
  });
  return index;
};

ext.checkSelectedItems =  (funcBlocks, funcBlocksIndex, selectedItems) => {
  funcBlocks.forEach((block) => {
    block.objs.forEach((obj) => {
      obj.isSelected = false;
    });        
  });
  selectedItems.forEach((item) => {
    funcBlocksIndex[item.funcId]
      .obj[item.objId].isSelected = true;
  });
};

ext.collapseBlocks = (funcBlocks) => {
  funcBlocks.forEach((block) => {
    block.isCollapsed = block.objs.every((obj) => {
      return obj.isSelected === false;
    });
  });
};

ext.addSelectedItem = (selectedItems, funcId, objId) => {
  selectedItems.push({
    funcId: funcId,
    objId: objId
  });
};

ext.remSelectedItem = (selectedItems, funcId, objId) => {
  var i, item;
  for (i = selectedItems.length - 1; i >= 0; i -= 1) {
      item = selectedItems[i];
      if (item.fimcId === funcId || item.objId === objId) {
          selectedItems.splice(i, 1);
      }
  }
};

ext.flushSelectedItemsToJiraField = (selectedItems) => {
    var jiraField = jQuery('#' + work_stuff_cf);
    jiraField.val(JSON.stringify(selectedItems));
}

module.exports = ext;
