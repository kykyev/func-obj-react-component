
var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');

var AppDispatcher = require('../dispatcher/AppDispatcher');
var AppConstants = require('../constants/AppConstants');
var helpers = require('./_AppStore');

var CHANGE_EVENT = 'change';


var AppStore = assign({}, EventEmitter.prototype, {

  _funcBlocks: [],
  _selectedItems: [],

  init: function(data) {
    this._funcBlocks = data.funcBlocks;
    this._selectedItems = data.selectedItems;
    this._funcBlocksIndex = helpers.buildFuncBlockIndex(this._funcBlocks);
    helpers.checkSelectedItems(this._funcBlocks,
            this._funcBlocksIndex, this._selectedItems);
    helpers.collapseBlocks(this._funcBlocks);
  },

  getFuncBlocks: function() { return this._funcBlocks; },
  getSelectedItems: function() { return this._selectedItems; },

  emitChange: function() {
    this.emit(CHANGE_EVENT);
  },

  addChangeListener: function(callback) {
    this.on(CHANGE_EVENT, callback);
  },

  selectObjItem: function(obj) {
    this._funcBlocksIndex[obj.funcId]
      .obj[obj.objId].isSelected = true;
    helpers.addSelectedItem(this._selectedItems, obj.funcId, obj.objId);
  },

  unselectObjItem: function(obj) {
    this._funcBlocksIndex[obj.funcId]
      .obj[obj.objId].isSelected = false;
    helpers.remSelectedItem(this._selectedItems, obj.funcId, obj.objId);
  },

  toggleFuncCollapse: function(funcId) {
    var block = this._funcBlocksIndex[funcId].block;
    block.isCollapsed = !block.isCollapsed;
  }
});

AppDispatcher.register(function(action) {
  switch(action.actionType) {
    case AppConstants.FUNC_TOGGLE_COLLAPSE:
      AppStore.toggleFuncCollapse(action.funcId);
      AppStore.emitChange();
      break;
    case AppConstants.OBJ_SELECT_ITEM:
      AppStore.selectObjItem(action.obj);
      AppStore.emitChange();
      break;
    case AppConstants.OBJ_UNSELECT_ITEM:
      AppStore.unselectObjItem(action.obj);
      AppStore.emitChange();
      break;
    case AppConstants.INITIAL_DATA_FROM_REMOTE_READY:
      AppStore.init(action.data);
      AppStore.emitChange();
      break;
    case AppConstants.SUBMIT_JIRA_FORM:
        helpers.flushSelectedItemsToJiraField(AppStore.getSelectedItems());
        break;
    default:
      // nothing
  }
});

module.exports = AppStore;
