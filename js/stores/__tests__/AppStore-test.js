
jest.dontMock('object-assign');
jest.dontMock('../AppStore');
jest.dontMock('../../constants/AppConstants');


describe('AppStore', function() {
  
  var AppConstants = require('../../constants/AppConstants');
  var AppDispatcher;
  var AppStore;
  var test_data = {};

  var actionInitialDataReady = {
    actionType: AppConstants.INITIAL_DATA_FROM_REMOTE_READY,
    data: test_data
  };

  beforeEach(() => {
    AppDispatcher = require('../../dispatcher/AppDispatcher');
    AppStore = require('../AppStore');
    callback = AppDispatcher.register.mock.calls[0][0];
  });

  it('initial state is empty', () => {
    var blocks = AppStore.getFuncBlocks();
    var selectedItems = AppStore.getSelectedItems();
    expect(blocks).toEqual([]);
    expect(selectedItems).toEqual([]);
  });
});
