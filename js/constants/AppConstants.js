
module.exports = {
  FUNC_TOGGLE_COLLAPSE: "FUNC_TOGGLE_COLLAPSE",
  INITIAL_DATA_FROM_REMOTE_READY: "INITIAL_DATA_FROM_REMOTE_READY",
  OBJ_SELECT_ITEM: "OBJ_SELECT_ITEM",
  OBJ_UNSELECT_ITEM: "OBJ_UNSELECT_ITEM",
  START_FETCH_INITIAL_DATA_FROM_REMOTE: "START_FETCH_INITIAL_DATA_FROM_REMOTE",
  SUBMIT_JIRA_FORM: "SUBMIT_JIRA_FORM",
  extra: {
  edit_form_submit_event: "kyky.event.edit_form_submit",
    func_obj_map_url: "/jira/plugins/servlet/webapp/sufd-worklog/t3map/json/fync-obj-map/?proj=SUFD",
    work_stuff_cf: "customfield_13407"
  }
};
